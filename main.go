package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"syscall/js"

	irma "github.com/privacybydesign/irmago"
	"github.com/sirupsen/logrus"
)

type Result struct {
	Disclosed   [][]*irma.DisclosedAttribute `json:"disclosed,omitempty"`
	ProofStatus irma.ProofStatus             `json:"proofStatus,omitempty"`
}

func parseConfiguration(path string) (*irma.Configuration, error) {
	fmt.Println("Parsing configuration.")
	conf, err := irma.NewConfiguration(path, irma.ConfigurationOptions{})
	if err != nil {
		return nil, err
	}

	if err := conf.ParseFolder(); err != nil {
		return nil, err
	}

	if err := conf.ValidateKeys(); err != nil {
		return nil, err
	}

	if len(conf.SchemeManagers) == 0 {
		// Only works if config is not ReadOnly.
		// Also need a writable filesystem (such as InMemory or Overlay) in BrowserFS, see `index.js`.
		fmt.Println("Specified folder doesn't contain any schemes, downloading instead.")
		if err := conf.DownloadDefaultSchemes(); err != nil {
			return nil, err
		}
	}

	fmt.Println("Configuration successfully parsed. Updating schemes.")
	conf.UpdateSchemes()

	for _, w := range conf.Warnings {
		fmt.Println("Warning: ", w)
	}

	return conf, err
}

func verify(conf *irma.Configuration, jsSig js.Value, jsReq js.Value) (js.Value, error) {
	// Make sure any uncaught JS errors are converted to Go errors.
	defer func() {
		e := recover()

		if e == nil {
			return
		}

		if jsErr, ok := e.(js.Error); ok {
			e = errors.New(jsErr.Error())
			return
		}

		panic(e)
	}()

	// Convert the JS inputs to Go inputs.
	signatureString := js.Global().Get("JSON").Call("stringify", jsSig).String()
	signature := &irma.SignedMessage{}
	if err := json.Unmarshal([]byte(signatureString), signature); err != nil {
		return js.Null(), err
	}

	var request *irma.SignatureRequest
	if !jsReq.IsUndefined() {
		requestString := js.Global().Get("JSON").Call("stringify", jsReq).String()
		request := &irma.SignatureRequest{}
		if err := json.Unmarshal([]byte(requestString), request); err != nil {
			return js.Null(), err
		}
	}

	// Verify the signature.
	disclosed, status, err := signature.Verify(conf, request)
	if err != nil {
		return js.Null(), err
	}

	// Convert the output back to a js.Value (object in this case).
	outSerialized, err := json.Marshal(Result{Disclosed: disclosed, ProofStatus: status})
	if err != nil {
		return js.Null(), err
	}

	result := js.Global().Get("JSON").Call("parse", string(outSerialized))
	return result, nil
}

func verifyWrapper(conf *irma.Configuration) js.Func {
	verifyFunc := js.FuncOf(func(this js.Value, args []js.Value) interface{} {

		handler := js.FuncOf(func(this js.Value, handlerArgs []js.Value) interface{} {
			resolve := handlerArgs[0]
			reject := handlerArgs[1]
			jsError := js.Global().Get("Error")

			go func() {
				if !(len(args) == 1 || len(args) == 2) {
					reject.Invoke(jsError.New("wrong number of arguments"))
					return
				}

				sig := args[0]
				var req js.Value
				if len(args) == 2 {
					req = args[1]
				}

				out, err := verify(conf, sig, req)
				if err != nil {
					reject.Invoke(jsError.New(err.Error()))
					return
				}

				resolve.Invoke(out)
			}()

			return nil
		})

		jsPromise := js.Global().Get("Promise").New(handler)
		return jsPromise
	})

	return verifyFunc
}

func main() {
	logger := logrus.New()
	logger.Level = logrus.DebugLevel
	irma.SetLogger(logger)

	path := "/irma_configuration"

	conf, err := parseConfiguration(path)
	if err != nil {
		panic(err)
	}

	js.Global().Set("verifySignature", verifyWrapper(conf))

	<-make(chan bool)
}
