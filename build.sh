#!/bin/sh

mkdir -p dist
rm -rf dist/*

git clone https://github.com/privacybydesign/irma-demo-schememanager.git irma-demo
git clone https://github.com/privacybydesign/pbdf-schememanager pbdf

rm -rf irma-demo/.git*
rm -rf pbdf/.git*

zip -r dist/schemes.zip irma-demo pbdf
rm -rf irma-demo pbdf

go mod vendor
git apply vendor.patch

GOOS=js GOARCH=wasm go build -o dist/main.wasm
wasm-opt -Os dist/main.wasm -o dist/main_opt.wasm

rm dist/main.wasm

cp index.html index.js wasm_exec.js dist/
