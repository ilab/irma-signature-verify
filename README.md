# irma-browser-verify

Verify irma signatures in the browser.

## Contents

- `main.go`: go source to create a wasm module that verifies signatures.
- `wasm_exec.js`: adaptation of [Go author's wasm_exec](https://github.com/golang/go/blob/master/misc/wasm/wasm_exec.js). Exports functions that run a WASM module.
- `index.html`: main html page that loads browserFS, `wasm_exec.js` and finally `index.js`.
- `index.js`: main application code. Sets up a filesystem and runs the wasm module.
- `vendor.patch`: a patch to apply after vendoring the dependencies of `main.go` to make it compile to the `GOOS=js GOARCH=wasm` target.
- `build.sh`: build script.

## Requirements 
- `go` (tested using go 1.17),
- `wasm-opt`, included in the binaryen project,
- `git`.

## Building

Run `build.sh` to build a proof of concept.

Statically host `/dist` and open it in the browser. The wasm module adds
a function `verifySignature(sig, req)` added to the global scope (e.g.
`window`, `globalThis`). The function is asynchronous and take signatures as
Javascript object. The request is optional.

## Example

As an example, we create a signature request using the following session
request, asking a user to sign a message using a (demo) e-mail credential.

```json
{
  "@context": "https://irma.app/ld/request/signature/v2",
  "message": "Message to be signed by user",
  "disclose": [[["irma-demo.sidn-pbdf.email.email"]]]
}
```

After the session has completed, the following session result is retrieved from
the IRMA server. The result includes the `signature` field and some
verification that the server has done. The rest of this example verifies the
signature client-side, in the browser.

```json
{
  "token": "Txeg4blAXCPAd6QyO2AV",
  "status": "DONE",
  "type": "signing",
  "proofStatus": "VALID",
  "disclosed": [
    [
      {
        "rawvalue": "henk@example.com",
        "value": {
          "": "henk@example.com",
          "en": "henk@example.com",
          "nl": "henk@example.com"
        },
        "id": "irma-demo.sidn-pbdf.email.email",
        "status": "PRESENT",
        "issuancetime": 1652313600
      }
    ]
  ],
  "signature": {
    "@context": "https://irma.app/ld/signature/v2",
    "signature": [
      {
        "c": "csMLLMI4cApjJAjKUdlUyxTL+1NSH7gHjJHn0IoKsIc=",
        "A": "WuyDvT4zh6Zjxw277AeaTFGwEVVg6rnjZraCRYjabpzdLg29tKirboeBpPhktu/5g7g73Pfq9SqAz45/LiHijKIJfPy/eY5eYPMxJQmkVUZVNArTSysU20BOI/nWl3um0bKDADTyZVHaVu25T/D1bFVgLZJ/jbx3iRRmJT38dlk=",
        "e_response": "fSKVH6PVA8z5iqIw1jO+MLuFlpuIPgw3QcaDSwwp8UyJwu8ve8ikf34kajYk82eq4X4fAXnJZ4Vb",
        "v_response": "AkGqpzHxNAJSYJa2u8q2PHEU1Gsq8ySXV4B9bJFdnsnscCc2cvGFo1/d/Y4l+eUbwdU30sQsveYOmw0wiRr+U/+ZCeMT4acf9zpKF53SZIGRGDj2GhQWvZRsVBb4a0HvY3VJ6MjWhwWIKSiWSvBEuVV3WrH0t8wWaNG0r3iw5srJRSgetPOamo+egRgeH9R0RqCiR/WvjwPHqDbztE6giGLAxOuyL7mvS387n+U8fBZ6lQo3HDSmzYCxFULmd0jne5O9gmPq9bEG626LwMxg/8fcbluSMxOVjYMeRUk1qG1qYZnnXZ3Rxvp7XbY3jWYbxbXLaTf9Ggh+O7w004Kk",
        "a_responses": {
          "0": "TBXSrwvFPoYgJO80i50JPoHBQCbbYeZkjRru56v9bWr2/ezZxzKFPcwJrxjdxq1RmaBZp8AHW34ngCg6Zj47vb2qxK7xl1w1SQY=",
          "3": "S4XCeFpgJjOmAx8tDdGnVzltY+M5dx/KgKiddOOrtWit59LT165Q5P5JgqGgrH7Zo/ocucNALMh58xt8E+1SPt66/ECfAtC+31c="
        },
        "a_disclosed": {
          "1": "AwAKrAAaAABFPftngnXqdZeTfu2Omrh8",
          "2": "0Mrc1oDK8MLa4NjKXMbe2w=="
        },
        "rangeproofs": null
      }
    ],
    "indices": [
      [
        {
          "cred": 0,
          "attr": 2
        }
      ]
    ],
    "nonce": "GJswVqeVPiaZlTuG8ON8hw==",
    "context": "AQ==",
    "message": "Message to be signed by user",
    "timestamp": {
      "Time": 1652700858,
      "ServerUrl": "https://keyshare.privacybydesign.foundation/atumd/",
      "Sig": {
        "Alg": "ed25519",
        "Data": "0fkOu9pXl/1hoeyuMIJWTcVKSCPwVQUC5vo5R/yJDD6cLRiKzCSa/XoFlpkLlZ61x74XVVTmbID68gDumm2QBw==",
        "PublicKey": "MKdXxJxEWPRIwNP7SuvP0J/M/NV51VZvqCyO+7eDwJ8="
      }
    }
  }
}
```

Calling

```Javascript
const result = await verifySignature(sessionResult.signature, req)
```

using the request and result mentioned above yields the following verification results:

```json
{
  "disclosed": [
    [
      {
        "rawvalue": "henk@example.com",
        "value": {
          "": "henk@example.com",
          "en": "henk@example.com",
          "nl": "henk@example.com"
        },
        "id": "irma-demo.sidn-pbdf.email.email",
        "status": "EXTRA",
        "issuancetime": 1652313600
      }
    ]
  ],
  "proofStatus": "VALID"
}
```

This include the attributes that were used to sign the message and whether or
not the signature was valid.

## TODOs

- Try tinygo after xml serialization is supported (see, https://tinygo.org/docs/reference/lang-support/stdlib/#encodingxml). This should dramatically decrease WASM module size.
- Several locations do not yet allow accessing via the browser and require CORS configuration:
  - [x] the scheme locations (https://privacybydesign.foundation/schememanager),
  - [ ] sidn atum server (https://irma.sidn.nl/atumd/checkPublicKey),
- BrowserFS seems unmaintained. Several other options exist, such as [lightning-fs](https://github.com/isomorphic-git/lightning-fs).
- Cache the atum public keys (offloads the server). Can be implemented using localStorage or the [Cache API](https://developer.mozilla.org/en-US/docs/Web/API/Cache).
- Test in several browsers (currently only tested on Chrome and Firefox).
- Optimize.
