(async function () {
  if (!WebAssembly.instantiateStreaming) {
    WebAssembly.instantiateStreaming = async (resp, importObject) => {
      const source = await (await resp).arrayBuffer();
      return await WebAssembly.instantiate(source, importObject);
    };
  }

  const schemes = await fetch("schemes.zip");
  const schemesBuffer = await schemes.arrayBuffer();
  const Buffer = BrowserFS.BFSRequire("buffer").Buffer;

  BrowserFS.configure(
    {
      fs: "OverlayFS",
      options: {
        readable: {
          fs: "MountableFileSystem",
          options: {
            "/irma_configuration": {
              fs: "ZipFS",
              options: {
                zipData: Buffer.from(schemesBuffer),
              },
            },
          },
        },
        writable: {
          fs: "InMemory",
        },
      },
    },
    async (e) => {
      if (e) throw e;
      console.log("BrowserFS configured.");

      const go = new Go();
      const result = await WebAssembly.instantiateStreaming(
        fetch("main_opt.wasm"),
        go.importObject
      );
      go.run(result.instance).catch((e) => console.error(err));
    }
  );
})();
